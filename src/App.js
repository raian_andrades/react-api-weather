import React from 'react';
import Titles from './components/Titles';
import Form from './components/Form';
import Weather from './components/Weather';

const API_KEY = "13efeaeab5d8e4400e32e79fb749c686";
//http://api.openweathermap.org/data/2.5/weather?q=London,uk&appid=13efeaeab5d8e4400e32e79fb749c686&units=metric

//initialize the component
class App extends React.Component {

  // constructor(props) {
  //     super(props);
  //     this.state = {
        
  //     }
  // }

  state = {
      temperature: undefined,
      city: undefined,
      country: undefined,
      humidity: undefined,
      description: undefined,
      error: undefined
  }

    getWeather = async (e) => {
        e.preventDefault(); //prevent to reload when press the button - single plage application
        const city = e.target.elements.city.value;
        const country = e.target.elements.country.value;
        const api_call = await fetch(`http://api.openweathermap.org/data/2.5/weather?q=${city},${country}&appid=${API_KEY}&units=metric`);
        const data = await api_call.json();
        console.log(data);
        if(city && country){
          this.setState({
            temperature: data.main.temp,
            city: data.name,
            country: data.sys.country,
            humidity: data.main.humidity,
            description: data.weather[0].description,
            error: ""
          });
        } else {
          this.setState({
            temperature: undefined,
            city: undefined,
            country: undefined,
            humidity: undefined,
            description: undefined,
            error: "Please enter the value."
          });
        }
    }

    //display content inside components
    render() {
        return (
            <div>
                <div className="wrapper">
                    <div className="main">
                        <div className="container">
                            <div className="row">
                                <div className="col-xs-5 title-container">
                                    <Titles />
                                </div>
                                <div className="col-xs-7 form-container">
                                    <Form getWeather={this.getWeather} />
                                    <Weather 
                                      temperature={this.state.temperature} 
                                      city={this.state.city} 
                                      country={this.state.country} 
                                      humidity={this.state.humidity} 
                                      description={this.state.description}
                                      error={this.state.error} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
};


export default App;